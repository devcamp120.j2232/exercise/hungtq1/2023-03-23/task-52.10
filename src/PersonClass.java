import task52_10.Person;

public class PersonClass{
       

    public static void main(String[] args) throws Exception {
        System.out.println("Hello, World!");

        Person person1 = new Person();
        Person person2 = new Person("Hung", "Tran", "male", "098896976", "hung@gami.com");
 
        System.out.println("Person1: " + person1.toString());
        System.out.println("Person2: " + person2.toString());
        System.out.println("Person2: " + person2.getFirstName());

    }
}
